from django.db import models
from django.contrib import admin

class User(models.Model):
    name = models.CharField(max_length=200)
    password = models.CharField(max_length=200)

class Role(models.Model):
    user = models.ForeignKey(User)
    descriptioon = models.CharField(max_length=200)

class OS(models.Model):
    name = models.CharField(max_length=50)
    version = models.CharField(max_length=20)

class Shortcuts(models.Model):
    os = models.ForeignKey(OS)
    programName = models.CharField(max_length=25)
    shortcut = models.CharField(max_length=20)
    description = models.CharField(max_length=20)

admin.site.register(User)
admin.site.register(Role)
admin.site.register(OS)
admin.site.register(Shortcuts)
