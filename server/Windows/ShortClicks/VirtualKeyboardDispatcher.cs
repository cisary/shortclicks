﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsInput;

namespace ShortClipsConsole
{
    class VirtualKeyboardDispatcher
    {
        internal static void startDispatching()
        {
            while (true)
            {
                string s = Console.ReadLine();
                if (s == null)
                {
                    continue;
                }
                Console.WriteLine(s);
                if (s.Equals("exit"))
                {
                    Environment.Exit(0);
                }
                if (s.Equals("ALT-TAB"))
                {
                    //read string, dispatch shortcut to windows..
                    //Alt+tab
                    InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.MENU, VirtualKeyCode.TAB);
                }
                //SendMessage(currentProccess.MainWindowHandle, 0x0102, 0x43, null);
                //SendMessage(currentProccess.MainWindowHandle, 0x0102, 0x43, null);
                //Console.WriteLine("Command: " + s);
                //keybd_event(VK_CANCEL, 0, KEYEVENTF_EXTENDEDKEY, 0);
                //keybd_event(VK_CANCEL, 0, KEYEVENTF_KEYUP, 0); 

            }
        }
    }
}
